﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Game
{
    public List<Card> EnemyDeck, PlayerDeck,
                      EnemyHand, PlayerHand,
                      EnemyField, PlayerField;

    public Game()
    {
        EnemyDeck = GiveDeckCard();
        PlayerDeck = GiveDeckCard();

        EnemyHand = new List<Card>();
        PlayerHand = new List<Card>();

        EnemyField = new List<Card>();
        PlayerField = new List<Card>();
    }

    List<Card> GiveDeckCard()
    {
        List<Card> list = new List<Card>();
        for(int i = 0; i < 15; i++)
        {
            list.Add(CardManagerScr.AllCards[UnityEngine.Random.Range(0, CardManagerScr.AllCards.Count)]);
        }

        return list;
    }
}



public class GameManagerScr : MonoBehaviour {

    public Game CurrentGame;
    public Transform EnemyHand, PlayerHand;
    public Transform EnemyDeck, PlayerDeck;
    public GameObject CardPref;
    public int Turn, TurnTime = 30;
    public TextMeshProUGUI TurnTimeTxt;
    public Button EndTurn;
    public TextMeshProUGUI MyDeckTxt;
    public TextMeshProUGUI EnemyDeckTxt;
    public TextMeshProUGUI PlayerMannaTxt;
    public TextMeshProUGUI EnemyMannaTxt;
    public int PlayerManna, EnemyManna;
    public bool IsWin;
    public DropPlease DropScript;
    public bool IsPlayerTurn
    {
        get
        {
            return Turn % 2 == 0;
        }
    }

    void Start()
    {
        Turn = 0;
        PlayerManna = 0;
        EnemyManna = 0;

        if (IsPlayerTurn || Turn == 0)
        {
            PlayerManna++;
            PlayerMannaTxt.text = PlayerManna.ToString();
            EnemyMannaTxt.text = EnemyManna.ToString();
        }
        else
        {
            EnemyManna++;
            EnemyMannaTxt.text = EnemyManna.ToString();
        }
        CurrentGame = new Game();

        GiveHandCard(CurrentGame.EnemyDeck, EnemyHand);
        GiveHandCard(CurrentGame.PlayerDeck, PlayerHand);
        int startCard = 11;
        MyDeckTxt.text = startCard.ToString();
        EnemyDeckTxt.text = startCard.ToString();

        StartCoroutine(TurnFunc());
    }

    void GiveHandCard(List<Card> deck, Transform hand)
    {
        int i = 0;
        while(i++ < 4)
        {
            GiveCardToHand(deck, hand);
        }
    }

    IEnumerator TurnFunc()
    {
        TurnTime = 30;
        TurnTimeTxt.text = TurnTime.ToString();

        if (IsPlayerTurn)
        {
            while(TurnTime-- > 0)
            {
                TurnTimeTxt.text = TurnTime.ToString();
                yield return new WaitForSeconds(1);
            }
        }
        else
        {
            while(TurnTime-- > 25)
            {
                TurnTimeTxt.text = TurnTime.ToString();
                yield return new WaitForSeconds(1);
            }
        }
        ChangeTurn();
    }

    private int WhatTurn()
    {
        return Turn;
    }

    void GiveCardToHand(List<Card> deck, Transform hand)
    {
        if (deck.Count == 0)
        {
            return;
        }

        Card card = deck[0];

        int a = deck.Count - 1;
        if (Turn % 2 == 0)
        {
            MyDeckTxt.text = a.ToString();
        }
        else
        {
            EnemyDeckTxt.text = a.ToString();
        }
        
        GameObject CardGO = Instantiate(CardPref, hand, false);

        if(hand == EnemyHand)
        {
            CardGO.GetComponent<CardInfo>().HideCardInfo(card);
        }
        else
        {
            CardGO.GetComponent<CardInfo>().ShowCardInfo(card);
        }

        deck.RemoveAt(0);
    }

    public void ChangeTurn()
    {
        StopAllCoroutines();
        Turn++;
        if (IsPlayerTurn)
        {
            PlayerManna++;
            PlayerMannaTxt.text = PlayerManna.ToString();
        }
        else
        {
            EnemyManna++;
            EnemyMannaTxt.text = EnemyManna.ToString();
        }
        EndTurn.interactable = IsPlayerTurn;

        GiveNewCards();


        StartCoroutine(TurnFunc());
    }

    private void GiveNewCards()
    {
        if (IsPlayerTurn)
        {
            GiveCardToHand(CurrentGame.PlayerDeck, PlayerHand);
        }
        else
        {
            GiveCardToHand(CurrentGame.EnemyDeck, EnemyHand);
        }
    }
}