﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Card
{
    public string Name;
    public Sprite Logo;
    public int Attack, Defense, Mana;

    public Card(string name, string logoPath, int attack, int defense, int mana)
    {
        Name = name;
        Logo = Resources.Load<Sprite>(logoPath);
        Attack = attack;
        Defense = defense;
        Mana = mana;
    }

}

public static class CardManagerScr
{
    public static List<Card> AllCards = new List<Card>();
}

public class CardManager : MonoBehaviour
{
    public void Awake()
    {
        CardManagerScr.AllCards.Add(new Card("Carta 1", "Sprite/Cards/card_1", 1, 1, 1));
        CardManagerScr.AllCards.Add(new Card("Carta 2", "Sprite/Cards/card_2", 2, 1, 1));
        CardManagerScr.AllCards.Add(new Card("Carta 3", "Sprite/Cards/card_3", 1, 2, 1));
        CardManagerScr.AllCards.Add(new Card("Carta 4", "Sprite/Cards/card_4", 3, 1, 2));
        CardManagerScr.AllCards.Add(new Card("Carta 5", "Sprite/Cards/card_5", 4, 2, 3));
        CardManagerScr.AllCards.Add(new Card("Carta 6", "Sprite/Cards/card_6", 6, 3, 4));
    }
}
