﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinScript : MonoBehaviour
{

    public Button RestoreBtn;

    void Start()
    {
        Button restoreButton = RestoreBtn.GetComponent<Button>();
        restoreButton.onClick.AddListener(restoreLvl);
    }

    private void restoreLvl()
    {
        SceneManager.LoadScene(0);
    }
}
