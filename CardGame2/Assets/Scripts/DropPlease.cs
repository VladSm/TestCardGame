﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class DropPlease : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Camera MainCamera;
    public int Cards;
    public GameObject WinCanvas;
    public enum FieldType
    {
        SELF_HAND,
        SELF_FIELD,
        ENEMY_HAND,
        ENEMY_FIELD

    }

    void Awake()
    {
        MainCamera = Camera.allCameras[0];
    }
    public FieldType Type;

    public void OnDrop(PointerEventData eventData)
    {
        if (Type != FieldType.SELF_FIELD)
        {
            return;
        }
        CardInfo cardManager = eventData.pointerDrag.GetComponent<CardInfo>();
        CardScript card = eventData.pointerDrag.GetComponent<CardScript>();
        GameManagerScr gameManager = MainCamera.GetComponent<GameManagerScr>();
        if (card && gameManager.PlayerManna >= cardManager.Mana)
        {
            card.DefaultParent = transform;
            Cards++;
        }

        if(Cards == 6)
        {
            WinCanvas.SetActive(true);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null || Type == FieldType.ENEMY_FIELD || Type == FieldType.ENEMY_HAND)
            return;

        CardScript card = eventData.pointerDrag.GetComponent<CardScript>();

        if (card)
        {
            card.DefaultTempCardParent = transform;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
            return;

        CardScript card = eventData.pointerDrag.GetComponent<CardScript>();

        if (card && card.DefaultTempCardParent == transform)
            card.DefaultTempCardParent = card.DefaultParent;
    }
}