﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardInfo : MonoBehaviour
{
    public Card SelfCard;
    public Image Logo;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Manna;
    public TextMeshProUGUI Health;
    public TextMeshProUGUI Attack;
    public int Mana;
    public void HideCardInfo(Card card)
    {
        SelfCard = card;
        Logo.sprite = null;
        Name.text = "";
        Manna.text = "";
        Health.text = "";
        Attack.text = "";
    }
    public void ShowCardInfo(Card card)
    {
        SelfCard = card;
        Logo.sprite = card.Logo;
        Logo.preserveAspect = true;
        Name.text = card.Name;
        Manna.text = card.Mana.ToString();
        Mana = WhatMana(card.Mana);
        Health.text = card.Defense.ToString();
        Attack.text = card.Attack.ToString();
    }

    public int WhatMana(int mana)
    {
        return mana;
    }
    public void Start()
    {
        //ShowCardInfo(CardManagerScr.AllCards[transform.GetSiblingIndex()]);
    }
}
